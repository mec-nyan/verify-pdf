import "./style.scss";
import { useState, useEffect, useRef } from "react/cjs/react.development";

function App() {
  const [file, setFile] = useState("...");

  function handleChange(e) {
    console.log(e.target.value);
    setFile(e.target.value);
  }

  return (
    <>
      <h1>A pdf app...</h1>
      <div className="outer">
        <label for="file">{"YOUR FILE, DUDE! >>>"}</label>
        <input id="file" type="file" accept=".pdf" onChange={handleChange} />

        <div className="input">
          <span>{file}</span>
        </div>
      </div>
    </>
  );
}

export default App;
